#!/bin/bash

# update jbpm configuration 
./updateConfig.groovy

# start jbpm
cd jbpm-installer; ant start.demo.noeclipse

# start tomcat
sh ${CATALINA_HOME}/bin/catalina.sh run
