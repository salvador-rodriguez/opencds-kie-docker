#!/usr/bin/env groovy

import groovy.xml.XmlUtil

def buildFile  = new File ("/jbpm-installer/build.xml")
def standaloneFile = new File("/jbpm-installer/wildfly-8.2.1.Final/standalone/configuration/standalone-full.xml")
def localhost = "localhost"
def portOffset = 1

/**
 * update build.xml
 */
def xmlBuildFile = new XmlSlurper(false, false).parse(buildFile)
def addr = InetAddress.getLocalHost()

java.net.InetAddress[] addresses=InetAddress.getAllByName(addr.getHostName())

for (address in addresses) {
	if (!(address.getHostAddress().startsWith("0"))) {
		localhost = address.getHostAddress()
	}
}

xmlBuildFile.property.find{
	if (it.@name == "jboss.bind.address"){
		println "set localhost value: ${localhost}"
		it.@value = localhost
	}
}

xmlBuildFile.target.waitfor.socket.find{
	if (it.@port == "8080"){
		println "set check jboss start port: 808${portOffset}"
		it.@port = "808" + portOffset
	}
}

XmlUtil.serialize(xmlBuildFile, new FileWriter(buildFile))


/**
 * update standalone.xml
 */
def xmlStandaloneFile = new XmlSlurper(false, false).parse(standaloneFile)

xmlStandaloneFile.interfaces.interface.'inet-address'.find{
	if (it.@value == '${jboss.bind.address.management:127.0.0.1}'){
		println "set 'jboss.bind.address.management' value: ${localhost}"
		it.@value = '${jboss.bind.address.management:' + "${localhost}" + "}"
	}
	if (it.@value == '${jboss.bind.address:127.0.0.1}'){
		println "set 'jboss.bind.address' value: ${localhost}"
		it.@value = '${jboss.bind.address:' + "${localhost}" + "}"
	}
	if (it.@value == '${jboss.bind.address.unsecure:127.0.0.1}'){
		println "set 'jboss.bind.address.unsecure' value: ${localhost}"
		it.@value = '${jboss.bind.address.unsecure:' + "${localhost}" + "}"
	}
}


/**
 * enable git repositories for remote cloning
 */
xmlStandaloneFile.extensions + {
	'system-properties' {
		println "add system property 'org.uberfire.nio.git.daemon.host: '${localhost}"
		property(name:'org.uberfire.nio.git.daemon.host', value:"${localhost}")
		println "add system property 'org.uberfire.nio.git.ssh.host: '${localhost}"
		property(name: 'org.uberfire.nio.git.ssh.host', value:"${localhost}")
	}
}



/**
 * change port-offset
 */
xmlStandaloneFile.'socket-binding-group'.find {
	if (it.@'port-offset' == '${jboss.socket.binding.port-offset:0}'){
		println "set 'port-offset' value: ${portOffset}"
		it.@'port-offset' = '${jboss.socket.binding.port-offset:' + "${portOffset}" + "}" 
	}	
}

XmlUtil.serialize(xmlStandaloneFile, new FileWriter(standaloneFile))

