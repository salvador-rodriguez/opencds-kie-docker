build image :
sudo docker build -t="opencds/opencds-kie" .

create volume for maven repository 
sudo docker create -v /root/.m2 --name opencds-kie-maven opencds/opencds-kie /bin/true

create volume for webapps
sudo docker create -v /usr/local/tomcat/webapps --name opencds-kie-webapps opencds/opencds-kie /bin/true

create container
sudo docker run --volumes-from opencds-kie-webapps --volumes-from opencds-kie-maven --restart=always -d -p 8081:8080 -p 8082:8081 --name opencds-kie opencds/opencds-kie

check your browser:
kie: 		http://localhost:8082/jbpm-console
opencds: 	http://localhost:8081/opencds-decision-support-service/evaluate

