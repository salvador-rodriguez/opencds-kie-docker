# Version: 0.0.1

FROM ubuntu:16.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# this is a non-interactive automated build - avoid some warning messages
ENV DEBIAN_FRONTEND noninteractive

# Install packages
ENV REFRESHED_AT 2016-08-12
RUN apt-get update && \
    apt-get install -yq --no-install-recommends curl git maven ant wget unzip software-properties-common pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Install Java
RUN wget --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jdk-8u101-linux-x64.tar.gz
RUN mkdir /usr/local/java
RUN tar -zxf jdk-8u101-linux-x64.tar.gz -C /usr/local/java
RUN rm -f jdk-8u101-linux-x64.tar.gz

ENV JAVA_HOME /usr/local/java/jdk1.8.0_101
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Install Groovy 
ENV GROOVY_VERSION 2.4.7
RUN wget http://dl.bintray.com/groovy/maven/apache-groovy-binary-${GROOVY_VERSION}.zip && \
	mkdir -p /opt/groovy && \
	unzip apache-groovy-binary-${GROOVY_VERSION}.zip -d /opt/groovy && \
	ln -s /opt/groovy/groovy-${GROOVY_VERSION} /opt/groovy/current && \
	rm apache-groovy-binary-${GROOVY_VERSION}.zip && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV GROOVY_HOME /opt/groovy/groovy-${GROOVY_VERSION}
RUN echo PATH=$GROOVY_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH GROOVY_HOME >> ~/.bashrc

# Set maven home
RUN echo export M2_HOME=/usr/share/maven >> ~/.bashrc

# Add jbpm-installer
ENV JBPM_INSTALLER jbpm-6.4.0.Final-installer-full.zip

COPY files/kie/${JBPM_INSTALLER} /
RUN unzip ${JBPM_INSTALLER}
RUN rm ${JBPM_INSTALLER}
COPY files/img_scripts/updateConfig.groovy /
RUN chmod +x updateConfig.groovy
RUN cd jbpm-installer; ant install.demo.noeclipse

# Install TOMCAT
ENV TOMCAT_MAJOR_VERSION 9
ENV TOMCAT_MINOR_VERSION 9.0.0.M9
ENV CATALINA_HOME /usr/local/tomcat

RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

COPY files/tomcat/tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
COPY files/tomcat/setenv.sh ${CATALINA_HOME}/bin/setenv.sh

# Add .keystore
COPY files/tomcat/.keystore ${CATALINA_HOME}/.keystore

# Configure https
RUN sed -i "s#</Server>##g" ${CATALINA_HOME}/conf/server.xml; \
        sed -i "s#  </Service>##g" ${CATALINA_HOME}/conf/server.xml; \
        echo '    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true" maxThreads="150" scheme="https" secure="true" clientAuth="false" sslProtocol="TLS" keystoreFile=".keystore" keystorePass="opencds" />' >> ${CATALINA_HOME}/conf/server.xml; \
        echo '  </Service>' >> ${CATALINA_HOME}/conf/server.xml; \
        echo '</Server>' >> ${CATALINA_HOME}/conf/server.xml

# Install OpenCDS
ENV OPENCDS_DECISION_SUPPORT_SERVICE_VERSION drools63
ENV OPENCDS_KNOWLEDGE_REPOSITORY_DATA_VERSION 2.1.3-SNAPSHOT

COPY files/opencds/opencds.properties /root/.opencds/opencds.properties
COPY files/opencds/sec.xml /root/.opencds/sec.xml
# Copy knowledge repository
COPY files/opencds/opencds-knowledge-repository-data-${OPENCDS_KNOWLEDGE_REPOSITORY_DATA_VERSION}.jar /
RUN unzip opencds-knowledge-repository-data-${OPENCDS_KNOWLEDGE_REPOSITORY_DATA_VERSION}.jar -d /root/.opencds/opencds-knowledge-repository-data/
RUN rm opencds-knowledge-repository-data-${OPENCDS_KNOWLEDGE_REPOSITORY_DATA_VERSION}.jar
# Copy opencds war
COPY files/opencds/opencds-decision-support-service-${OPENCDS_DECISION_SUPPORT_SERVICE_VERSION}.war /
RUN unzip opencds-decision-support-service-${OPENCDS_DECISION_SUPPORT_SERVICE_VERSION}.war -d ${CATALINA_HOME}/webapps/opencds-decision-support-service
RUN rm -f opencds-decision-support-service-${OPENCDS_DECISION_SUPPORT_SERVICE_VERSION}.war

# Add opencds dependencies
ENV OPENCDS_COMMON_VERSION 2.1.3-SNAPSHOT
ENV OPENCDS_VMR_INTERNAL_VERSION 2.1.3-SNAPSHOT

COPY files/opencds/opencds-common-${OPENCDS_COMMON_VERSION}.jar /opencds-common-${OPENCDS_COMMON_VERSION}.jar
COPY files/opencds/opencds-vmr-1_0-internal-${OPENCDS_VMR_INTERNAL_VERSION}.jar /opencds-vmr-1_0-internal-${OPENCDS_VMR_INTERNAL_VERSION}.jar
RUN mvn install:install-file -Dfile=/opencds-common-${OPENCDS_COMMON_VERSION}.jar -DgroupId=org.opencds -DartifactId=opencds-common -Dversion=${OPENCDS_COMMON_VERSION} -Dpackaging=jar
RUN mvn install:install-file -Dfile=/opencds-vmr-1_0-internal-${OPENCDS_VMR_INTERNAL_VERSION}.jar -DgroupId=org.opencds -DartifactId=opencds-vmr-1_0-internal -Dversion=${OPENCDS_VMR_INTERNAL_VERSION} -Dpackaging=jar
RUN rm opencds-vmr-1_0-internal-${OPENCDS_VMR_INTERNAL_VERSION}.jar
RUN rm opencds-common-${OPENCDS_COMMON_VERSION}.jar

# Add hapi-fhir dependencies
ENV HAPI_FHIR_VERSION 1.6-SNAPSHOT

COPY files/hapi-fhir/hapi-fhir-base-${HAPI_FHIR_VERSION}.jar /
COPY files/hapi-fhir/hapi-fhir-structures-dstu3-${HAPI_FHIR_VERSION}.jar /
RUN mvn install:install-file -Dfile=/hapi-fhir-base-${HAPI_FHIR_VERSION}.jar -DgroupId=ca.uhn.hapi.fhir -DartifactId=hapi-fhir-base -Dversion=${HAPI_FHIR_VERSION} -Dpackaging=jar
RUN mvn install:install-file -Dfile=/hapi-fhir-structures-dstu3-${HAPI_FHIR_VERSION}.jar -DgroupId=ca.uhn.hapi.fhir -DartifactId=hapi-fhir-structures-dstu3 -Dversion=${HAPI_FHIR_VERSION} -Dpackaging=jar
RUN rm hapi-fhir-base-${HAPI_FHIR_VERSION}.jar
RUN rm hapi-fhir-structures-dstu3-${HAPI_FHIR_VERSION}.jar

# Add image scripts
ADD files/img_scripts/run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh

# Add VOLUMES to allow backup of config and databases
VOLUME  ["${CATALINA_HOME}/webapps", "/root/.opencds", "/root/.m2"]

EXPOSE 8080
EXPOSE 8081
EXPOSE 9991
EXPOSE 8001
EXPOSE 9418
EXPOSE 1043
EXPOSE 8082

CMD ["/usr/local/bin/run.sh"]
